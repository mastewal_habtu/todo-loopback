'use strict';

module.exports = function(Task) {
  // Ensure the uniqueness of description property
  Task.validatesUniquenessOf(
    'description', {message: 'A task with the same name already exists.'});

  // enforce owner id to be task userId
  Task.beforeRemote('create', function (context, task, next) {
    context.req.body.userId = context.req.accessToken.userId;

    next();
  });


};
