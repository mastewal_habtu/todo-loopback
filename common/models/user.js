'use strict';

module.exports = function(User) {
  // check if a user exists based on username and return boolean indicating it
  User.username_exists = function(username, cb) {
    User.findOne({where: {username: username}}, function(error, user) {
      if (error) {
        cb(error, null);
      } else {
        if (user) {
          cb(null, true);
        } else {
          cb(null, false);
        }
      }
    });
  };

  User.remoteMethod(
    'username_exists',
    {
      http: {path: '/username_exists', verb: 'get'},
      accepts: {arg: 'username', type: 'string', required: true},
      returns: {arg: 'exists', type: 'boolean'},
    });
};
